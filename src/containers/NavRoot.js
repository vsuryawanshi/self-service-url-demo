import React, {Component} from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./Home";
import Notfound from "./NotFound";

export default class NavRoot extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <main>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="*" component={Notfound} />
                </Switch>
            </main>
        );
    }
}
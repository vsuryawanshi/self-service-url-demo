import React, {Component} from "react";

export default class BlueHeader extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="blue-header-wrap">
                <div className="width-wrap">
                    <div className="brand">Witsy.ai</div>
                    <div className="brand second">About Us</div>
                    <div className="search-wrap">
                        <input className="search-inp" type="text" placeholder="Search by people and content"/>
                    </div>
                    <div className="icons">
                        <div className="micon">
                        <div className="notif-count">9</div>
                            <img src={require("../images/alarm.svg")}/>
                        </div>
                        <div className="micon">
                            <div className="notif-count">3</div>
                            <img src={require("../images/envelope.svg")}/>
                        </div>
                        <div className="micon">
                            <img src={require("../images/cog.svg")}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
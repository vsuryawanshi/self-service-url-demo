import React, { Component } from "react";
import axios from "axios";

const TYPES = [
    {
        name:"Sensor Fusion",
        color:"rgb(150, 49, 231)"
    },
    {
        name:"Semantic Segmentation",
        color:"rgb(226, 62, 124)"
    },
    {
        name:"Cuboids",
        color:"rgb(60, 192, 167)"
    },
    {
        name:"Polygons",
        color:"rgb(255, 118, 0)"
    },
    {
        name:"2D Boxes",
        color:"rgb(99, 179, 28)"
    },
    {
        name:"Lines & Splines",
        color:"rgb(31, 119, 224)"
    }
];

const IMGS = [
    "https://media.istockphoto.com/photos/misty-summer-mountain-hills-landscape-picture-id509636590?k=6&m=509636590&s=612x612&w=0&h=0n261AbJgmWI5cUWLUwpcRAbuGfmbWPF44tvPvErZXA=",
    "https://www.outdoorphotographer.com/images/stories/2016/may/landscapes/lead.jpg",
    "https://www.pandotrip.com/wp-content/uploads/2016/06/cornwall-Photo-by-Giuseppe-Milo.jpg",
    "https://images.pexels.com/photos/39811/pexels-photo-39811.jpeg?auto=compress&cs=tinysrgb&h=350",
    "https://static1.squarespace.com/static/564fce67e4b0991ab3090853/t/56d7f822356fb0d6f300c563/1456994339278/maui-driveway-design-chris-curtis-landscapes.jpg?format=2500w",
    "http://yosemitelandscapes.com/images/005.jpg",
    "http://www.landscapesbydk.com/images/slider/slide1.jpg",
    "https://d36tnp772eyphs.cloudfront.net/blogs/1/2014/10/teton-cabin-940x626.jpg"
];

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiCallInProgress:false,
            selectedTypeIndex:-1,
            selectedTabIndex:0,
            selectedImageIndex:-1,
            imgUrl:"",
            showJSONContent:false,
            currentImageData:null,
            showModal:false
        };
        this.imageUrlText = null;
    }

    makeApiCall(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://167.99.110.49:8000/predict?url=" + this.state.imgUrl
            }).then(response => {
                this.setState({
                    currentImageData:response.data,
                    apiCallInProgress:false
                })
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    validateUrlandMakeCall(){
        var pastedUrl = this.imageUrlText.value;
        this.setState({
            imgUrl:pastedUrl,
            showModal:false
        },()=>{
            this.makeApiCall();
        })
    }

    render() {
        return (
            <div className="home-wrapper">
                <div className="title">Meet our Products</div>
                <div className="subtitle">Our API provides access to human-powered data for hundreds of use cases.</div>

                <ul className="type-wrap">
                    {
                        TYPES.map((type,index)=>{
                            return(
                                <li 
                                    className={`type-item` + (this.state.selectedTypeIndex == index ? " active" : "")} 
                                    key={index}
                                    onClick={()=>{
                                        this.setState({
                                            selectedTypeIndex:index
                                        })
                                    }}>
                                    <div className="circle" style={{backgroundColor:type.color}}/>
                                    <div className="txt">{type.name}</div>
                                </li>
                            )
                        })
                    }
                </ul>

                <div className="main-section">
                    <div className="left-input">
                        <div className="image-input-container">
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c"/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext">Please choose an image</div>
                            </div>
                            <div className="default-images">
                                {
                                    IMGS.map((currentImg,index)=>{
                                        return(
                                            <div 
                                                className={`default-img` + (this.state.selectedImageIndex == index ? " active" : "")} 
                                                key={index} 
                                                style={{backgroundImage:"url(" + currentImg + ")"}}
                                                onClick={()=>{
                                                    this.setState({
                                                        selectedImageIndex:index,
                                                        imgUrl:IMGS[index]
                                                    },()=>{
                                                        this.makeApiCall();
                                                    });
                                                }}/>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="or">OR</div>
                        <button className="btn black" style={{marginRight:20}} onClick={()=>{
                            this.setState({
                                showModal:true
                            });
                        }}>Enter Image URL</button>
                        <button className="btn ">Upload Image</button>
                    </div>
                    <div className="result-container">
                        {
                            this.state.imgUrl != "" ? <div className="hv">Please hover on the image to see the results</div> : null
                        }
                        {
                            this.state.imgUrl !== "" ?
                            <div className="current-selected-image" style={{backgroundImage:"url(" + this.state.imgUrl + ")"}}>
                                {
                                    this.state.apiCallInProgress ?
                                    <div className="loader">
                                        
                                    </div>
                                    :
                                    null
                                }
                                {
                                    this.state.currentImageData != null && this.state.currentImageData.tags != null ?
                                    <div className="tags-container">
                                        {
                                            Object.keys(this.state.currentImageData.tags).map((k,index)=>{
                                                let currentObj = this.state.currentImageData.tags[k];
                                                var color = "#f00";
                                                var percent = 0;
                                                if(currentObj.score){
                                                    percent = (currentObj.score <= 1 ? (currentObj.score * 100).toFixed(2) : 100);
                                                    if(percent >= 40 && percent < 70){
                                                        color="#ff0"
                                                    } else if(percent >= 70){
                                                        color="#0f0";
                                                    }
                                                } else {
                                                    percent = "";
                                                }
                                                return(
                                                    <div className={`ppup x` + (index+1)} key={index}>
                                                        <div className="cname">{k}{(percent !== "" ? " (" + percent + "%)" : "")}</div>
                                                        {
                                                            currentObj.score ? 
                                                            <div>
                                                                <div className="prog">
                                                                    <div className="aprog" style={{width: percent +"%", backgroundColor:color}}/>
                                                                </div>
                                                                <div className="ptext">{currentObj.text}</div>
                                                            </div>
                                                            
                                                            :
                                                            <div className="reso">
                                                                {currentObj.text}
                                                            </div>
                                                        }
                                                        
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                    :
                                    null
                                }
                                
                                <button className="btn jbtn" onClick={()=>{
                                    this.setState({
                                        showJSONContent:true
                                    });
                                }}>JSON</button>
                                <div className={`json-container` + (this.state.showJSONContent ? " show" : "")}>
                                    <div className="titlebar">
                                        <div className="actions">
                                            <i className="b c" onClick={()=>{
                                                this.setState({
                                                    showJSONContent:false
                                                })
                                            }}/>
                                            <i className="b min"/>
                                            <i className="b max"/>
                                        </div>
                                        <div className="ttext">Response JSON</div>
                                        <div className="json-content">
                                                <pre className="javascript">
                                                    {JSON.stringify(this.state.currentImageData, null, 4)}
                                                </pre>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            :
                            null
                        }
                    </div>
                </div>
                <div className={`modal` + (this.state.showModal ? " show" : "")}>
                    <div className="mtitle">Enter Image URL</div>
                    <div className="mcontent">
                        <textarea className="imgurl" ref={node => this.imageUrlText = node} placeholder="Paste the image url here"/>
                    </div>
                    <div className="maction">
                        <button className="btn" onClick={()=>{
                            this.setState({
                                showModal:false
                            })
                        }} style={{marginRight:20}}>Cancel</button>
                        <button className="btn black" onClick={()=>{
                            this.validateUrlandMakeCall();
                        }}>Go</button>
                    </div>
                </div>
            </div>
        );
    }
}
import React, {Component} from "react";
import ReactDOM from "react-dom";

const MENU_ITEMS = [
    {
        name:"About Us",
        link:""
    },
    {
        name:"Contact Us",
        link:""
    },
    {
        name:"Login / Signup",
        link:""
    }
];

export default class Header extends Component{
    constructor(props){
        super(props);
    }

    setBackground(visible){
        if(visible){
            ReactDOM.findDOMNode(this.wrap).classList.add("active");
        } else {
            ReactDOM.findDOMNode(this.wrap).classList.remove("active");
        }
    }

    render(){
        return(
            <div className="header-wrap" ref={node => this.wrap = node}>
                <div className="container">
                    <div className="logo"/>
                    <div className="menu-items">
                        {
                            MENU_ITEMS.map((mitem,index)=>{
                                return(
                                    <div className="mitem" key={index}>
                                        {mitem.name}
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}